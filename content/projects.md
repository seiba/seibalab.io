---
title: "Projects"
pin: true
summary: "Some of the cool projects I've been involved in"
--- 

## Main Projects
---

### [Privacy Helper](https://gitlab.com/seiba/PrivacyHelper)

Started off as a project whilst I was completing my course at Monash University, this app was developed with the intention of educating users about the dangerous consequences of allowing device permissions to apps. The original version was not completely open-source, however following the completion of my coursework, all non open-source components were replaced.

### Enron Scandal Analysis

Another project that originated from my coursework, this analysis spanned across several months. I had worked with a friend to extract and process the dataset to produce network graphs and detailed sociometric statistics on the scandal. Unfortunately the source-code of the project cannot be shared as a means to prevent collusion.

## Hobby projects
---

### [Thinkpad T440p upgrade guide](https://gitlab.com/seiba/thinkpad-t440p-upgrade-guide)

With my strong love of open-source comes with interest on hardware with excellent support of the Linux operating system. After researching on potential devices, I was able to conclude that the Lenovo Thinkpad T440p best aligned with my interests. With support of a wide-range of upgrades (hence my guide), and a large community of enthusiasts, this pick was a no-brainer for me.  

## Open-source contributions
---

### Samsung Galaxy S2 Kernels

My first experience with building the Linux kernel and my first contribution to the XDA-developers community. Started off from sheer curiosity, my work was then shared with various individuals on the Galaxy S2 forums. These kernels had many advanced features that were not available in standard OEM setups.

### Samsung Galaxy S5 Kernels

My second contribution to the XDA-developers community. This time with a more powerful device compared to the Galaxy S2. I helped introduce some important performance improvements and tweaks for the aging and no longer supported device. Although my contributions were not as frequent, I was able to produce many helpful guides related to tweaking kernel parameters.

### Privacy with Open-Source Software (PwOSS)

For a short time I had contributed to a small community of open-source and privacy enthusiasts, mainly developing guides related to the Android operating system.

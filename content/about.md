---
title: "About me"
pin: true
summary: "Get to know more about myself"
---

### About myself
---

Data engineer currently residing in Melbourne, Australia. Outside of work I like to modify cars, watch anime, and follow Aussie-rules football (AFL).

### Experience
---

- Graduated from Monash University with a Bachelor of Computer Science 
- Technology Analyst at Infosys Ltd from 2019 to 2021
- Currently employed as a Data Engineer at Octopus Energy
- My main expertise is working on Spark ETL/ELT frameworks, pipelines and databases

### Contact
---

For all enquiries, please contact me via Linkedin.

---
title:  "MacOS update woes"
date: 2020-06-09
---

It's been a bit more than a week since my last post, and it seems my WFH streak is about to be broken...

The laptop I use for work is the 2019 Macbook Pro 15-inch, and so far it has been a treat for software development. It is accepted that MacOS is heaps better than Windows in this area, but I still prefer Linux overall (open-source, ya-da).

Anyways, so after my usual morning work activities, I decided to do a security update. Currently I'm on MacOS Mojave since my workplace is still working on software compatibility before migrating to Catalina (the latest one). And so I thought, surely if it's a security update, nothing much is going to change. 

Boy I was wrong. 

After logging in, I was greeted with freezing and restarting that happens not too long afterwards. A complete disaster. I can't do anything. One thing I learned about MacOS from using it: in Windows you have blue screen of deaths. In MacOS, you can get kernel panics (of deaths?). 

Perhaps what happened to me was just a rare edge case, and perhaps it was my fault for being too eager to update. When I update software, I have the latest is greatest mentality shared with many other developers. 

Thankfully, tech support has been helpful. I hope to be returning back to normal work soon once I take my laptop to a scheduled appointment at the office tomorrow. 

As always, stay safe,

Joshua


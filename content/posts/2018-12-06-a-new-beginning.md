---
title:  "A new beginning"
date: 2018-12-06
---

It seems that I almost forgot that my website existed :)

Anyways, I'll run through a few updates since my last post. 

I've passed my final year exams for uni not too long ago! Time seems to fly when you're enjoying your studies, and it has been a hell of a ride. I somehow managed to achieve excellent results in all my units (believe in yourself).

Thinking back about all of the things I learnt from uni, I am truly grateful for having such wonderful tutors and lecturers. 

Graduation ceremony will commence mid-2019, where I'll do another update post-graduation.

More exciting however is that I'll be starting my new graduate job early 2019. 

Signing out,
Joshua Lay


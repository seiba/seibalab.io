---
title:  "New Macbook replacement"
date: 2020-06-11
---

So about the issues I was facing with my Macbook... 

Turns out it was much more widespread than I thought it would be. Oof.

However not all of the news is displeasing though. 

In short, I got a replacement Macbook. The Macbook Pro 16-inch model, a much improved iteration of the Pro series. 

Whilst the screen is definitely bigger (and more pleasing to the eyes), it doesn't feel much bigger at all. 

Also, the magic keyboard is soooo much better than the butterfly switchs. Those were just plain awful. 

Unfortunately, I lost a large majority of my documents during this process. Painful yes, but not the end of the world. 

I'll give Apple another chance, but maybe I'll stay a little more cautious with the updates :) 

Signing off, 

Joshua

---
title:  "My WFH experience"
date: 2020-06-01
---

Hello, anybody there...

It seems that I have neglected my blog, but from today, this changes (I promise). 

Last year I participated in that graduate program I mentioned in my last post. It was mostly what I would describe as very generic. 

To be fair though, not everyone in my group had programming experience. Only to my knowledge that there were people there who have signed up wanting different roles!

Anyways, that's not very important. Fast-forward today, I am working for one of Australia's big four banks. I will keep this hidden for privacy sake, but so far it hasn't been too bad. 

We are stuck in a work-from-home (WFH) situtation, where whilst a pain for some, I haven't been minding the change at all. 

From where I live, it takes up to 1 hour one-way to get to the CBD. If you accumulate the time I've spent on public transport, it would equate to hours upon hours of wasted time :(

Currently typing this on my Lenovo Thinkpad T440p (that's another story for later).

Peace out,
Joshua

